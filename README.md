# Charity group of Emmanuel

Charity group of Emmanuel was established by Catholic student studying in Hanoi on December 12th, 2012. Its mission is to help the poor people living in mountainous remote areas in the North of Vietnam.

## Get it up and running
**Install pipenv and create virtual environments**
```
pip install -U pip pipenv
pipenv install
pipenv shell
```

**Running project**
```
./manage.py migrate
./manage.py loaddata dev_data.json
./manage.py runserver
```
**Open in your browser and login to /admin with admin/123456a@*


**Dumpdata with**

```
python manage.py dumpdata --natural-primary --natural-foreign > dev_data.json
```
